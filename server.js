var express = require('express');
var path = require('path');
var fs = require('fs');


var app = express();

var isProduction = process.env.NODE_ENV === 'production';
var port = isProduction ? process.env.PORT : 3001;
var publicPath = path.resolve(__dirname, 'public');

// We point to our static assets
app.use(express.static(publicPath));

// And run the server
app.listen(port, function () {
  console.log('Server running on port ' + port);
});

app.get('/api/containers', function(req, res) {
  console.log('in containers');
  fs.readFile('containers.json', function(err, data) {
    res.setHeader('Cache-Control', 'no-cache');
    console.log(JSON.parse(data));
    res.json(JSON.parse(data));
  });
});
