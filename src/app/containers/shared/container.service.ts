import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ApiService } from '../../core';

@Injectable()
export class ContainerService {
  path: String = '/containers';

  constructor(private apiService: ApiService) {
  }

  getContainers(): Observable<any[]> {
    return this.apiService.get(this.path);
  }
}
