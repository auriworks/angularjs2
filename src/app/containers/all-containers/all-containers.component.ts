import { Component, OnInit } from '@angular/core';

import { ContainerRowComponent } from './container-row.component';

//imports service that talks to the API service
import { ContainerService } from '../shared';

@Component({
  selector: 'all-containers',
  templateUrl: 'all-containers.component.html'
})
export class AllContainersComponent implements OnInit {
  containers: Array<any>;

  constructor(private containerService: ContainerService) { }

  //keep the constructor light and initialize the container service in the ngOnInit()
  ngOnInit() {
    this.containerService.getContainers()
        .subscribe(resp => this.containers = resp); //the subscribe() is called after the REST call is completed. In the meantime, the HTML template is rendered.
  }
}
