//component for displaying a row
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tr.container-row',
  templateUrl: 'container-row.component.html'
})
export class ContainerRowComponent implements OnInit {
  @Input() container;

  constructor() { }

  ngOnInit() { }
}
