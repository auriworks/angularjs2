//wrapper for the HTTP calls

//adds injectable annotation
import { Injectable } from '@angular/core';

import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/observable/throw';

@Injectable() //annotation to indiciate that DI is needed to create the module
export class ApiService {
  //this can be externalized using DI and opaqueToken (which allows it to be listing in the app.module.ts providers and an NPM build process can inject the right config
  private apiEndpoint: String = '/api';
  private headers: Headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });
//constructor with a dependency injection (adds HTTP) and creates a variable for it
  constructor(private http: Http) {
  }

  private getJson(response: Response) {
    return response.json();
  }

  private checkForError(response: Response): Response {
    if (response.status >= 200 || response.status < 300) {
      return response;
    } else {
      const error = new Error(response.statusText);
      error['response'] = response;
      console.log(error);
      throw error;
    }
  }


  get(path): Observable<any[]> {
    return this.http.get(`${this.apiEndpoint}${path}`, { headers: this.headers })
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }

  post(path, body) {
    return this.http.post(`${this.apiEndpoint}${path}`,
      JSON.stringify(body),
      { headers: this.headers })
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }

  delete(path) {
    return this.http.delete(`${this.apiEndpoint}${path}`, { headers: this.headers })
      .map(this.checkForError)
      .catch(err => Observable.throw(err))
      .map(this.getJson);
  }
}
