import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

//root component
import { AppComponent } from './app';

//import of all components that we have created
import {
  ContainersComponent,
  AllContainersComponent,
  ContainerRowComponent,
  ContainerNamePipe,
  ContainerService
} from './containers';
import { ApiService } from './core';

import { DashboardComponent } from './dashboard';

//routing (navigating) across components
import { routing } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    ContainersComponent,
    AllContainersComponent,
    ContainerRowComponent,
    ContainerNamePipe,

    DashboardComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing
  ],
  providers: [ //service that can be used by components
    ApiService,
    ContainerService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}

