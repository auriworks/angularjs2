import { Routes, RouterModule } from '@angular/router';

import { ContainersComponent } from './containers';
import { DashboardComponent } from './dashboard';

//array of objects to navigate in the menu
const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'containers',
    component: ContainersComponent
  },
  {//redirects to the root for the dashboard item instead of /dashboard
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

//module that gets a routing list
export const routing = RouterModule.forRoot(routes);
