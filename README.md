## demo-docker-ui

A simple project to demostrate some of Angular2 features.

### Prereqs

This project is the seed for a simple [Docker](https://www.docker.com/) dashboard.
Consequently, you **must** have Docker running on your machine for the backend to work correctly.
You can get Docker for your platform [here](https://www.docker.com/products/overview).

Once you have that installed, you can read up on how to create a few running containers on your machine,
or simply start one by running the following

```
docker run -t -i -P
```

This will start up a `bash` shell running inside Ubuntu on your machine.
Type `exit`, and repeat a few times.
I suggest you leave one just running in a terminal.

In another terminal run the following

```
docker ps -a
```

This will list **all** the containers that you created so far, including those that you `exit`-ed out of.
Now you have a good basis to use this project.

### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed version 5+
- Make sure you have NPM installed version 3+
- `WINDOWS ONLY` run `npm install -g webpack webpack-dev-server typescript` to install global dependencies
- run `npm install` to install dependencies

Open another terminal (so now you have 2; one with docker, and this one).
Run

```
node server.js
```

This will start up the backend that will interrogate Docker for all running containers.

#### Running the `angular2` frontend

- (In one terminal) `node server.js`
- (In another terminal) `npm start`
- Browse to http://localhost:3000

Keep the console open at all times.
If the `containers` page is not displaying all the containers that you expect, then you can test the same usin the following.


```
// in the browser console
window.fetch('/api/containers').then((s, e) => console.log(s.json().then((a,b) => console.log(a))));
```

If the above works, then all is well. Else the backed may not be up, or your Docker process may be in trouble. 


If you want to use another port (that is, other than 3000), open `package.json` file, then change port in `--port 3000` script

### Linting

This repository includes `tslint` as one of it's `dev` dependencies. Simply run `npm run lint` for linting

### Theme

Thanks to [modularcode/modular-admin-html](https://github.com/modularcode/modular-admin-html)

#### Caveats

`modular-admin-html` needs 2 things to work correctly

1. You need a _specific_ div in `index.html`. This will **NOT** work if you put it in `app.html`.
Look for the comment `<!-- Reference block for modular-admin theme -->` in `index.html`.
2. `app.js` that ships with `modular-admin-html` _assumes_ files to be in a certain location.
Make sure that the `setThemeState` function in `app.js` points to `/app/css` vs `/css,js`
